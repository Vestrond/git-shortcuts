# GIT shortcuts
# [Git Status]
alias gs='git status'
# [Git Now]
alias gn='git log -1 --pretty=short | grep ""'
# [Git Message]
alias gm='git commit -m'
alias gm!='git commit --no-verify -m'
# [Git amend]
alias ga='git commit --amend'
alias ga!='git commit --amend --no-verify'
alias gan='git commit --amend --no-edit'
alias gan!='git commit --amend --no-edit --no-verify'
# [Git Push]
alias gp='git push origin'
alias gp!='git push --no-verify origin'
# [Git Push Current]
alias gpc='git push origin HEAD'
alias gpc!='git push origin HEAD --no-verify'
# [Git Force Push Current]
alias gfpc='git push -f origin HEAD'
alias gfpc!='git push -f origin HEAD --no-verify'
# [Git Push Master]
alias gpm='git push origin master'
alias gpm!='git push origin master --no-verify'
# [Git Push Develop]
alias gpd='git push origin develop'
alias gpd!='git push origin develop --no-verify'
# [Git Branch]
alias gb='git checkout -b'
# [Git Checkout Master]
alias gcm='git checkout master'
# [Git Checkout Develop]
alias gcd='git checkout develop'
# [Git Checkout]
alias gc='git checkout'
# [Git All]
alias gall='git add --all'
# [Git NOPE]
alias gnope='git reset'
# [Git Discard All]
alias gda='git checkout -- .'
# [Git PICK]
alias gpick='git cherry-pick'
# [Git Rebase Interactive]
alias gri='git rebase --interactive'
# [Git Rebase Continue]
alias grc='git rebase --continue'
# [Git CONFigS]
alias gconfs!='git config --list --show-origin'
alias gconfs='git config --list'
# [Git CONFig]
alias gconf!='git config --list --show-origin | grep'
alias gconf='git config --list | grep'
# [Git StasH list]
alias gshl='git stash list'
# [Git StasH Apply]
alias gsha='git stash apply'
# [Get CuRrent Banch]
alias gcrb='git rev-parse --abbrev-ref HEAD 2>/dev/null || echo "Not a Git repository"'
# Show all these commands
alias ogit=$'cat ~/.git-shortcuts-rc | grep -v ogit | grep git | sed "s/alias / /" | sed "s/=/ => /" | sed "s/\'//g"'

# Git Shortcuts

**Git Shortcuts** is a bash tool that provides shortcuts and utilities to make it easier to use Git through the command line.

## Features

* **Push Shortcuts** -
	* `gpc` for `git push origin HEAD`
	* `gp` for `git push origin`
	* and more.
* **Commit Shortcuts** -
	* `gm` for `git commit -m`
	* `gfix` for git `commit -m "fix: {message} [{branchName as jira task}]"`
	* `gfeat` for `git commit -m "feat: {message} [{branchName as jira task}]"`.
* **Branch Shortcuts** -
	* `gb` for `git checkout -b`
	* `gcm` for `git checkout master`
	* `gcd` for `git checkout develop`
	* `gc` for `git checkout`.
* **Other Shortcuts** -
	* `gs` for `git status`
	* `gall` for `git add --all`
	* `gda` for `git checkout -- .`
	* and more.

Additionally, there are complex functions that can be used to get the Jira task name from the branch name, debug your commit line, and more.

## Installation

To install Git Shortcuts, simply run the following command:

```bash
./install.sh
```

This will create a symbolic link to the **git-shortcuts.sh** and **git-shortcuts-functions.sh** file in your home directory.

## Usage

To use Git Shortcuts, simply open a terminal and type one of the shortcuts provided by the tool.

For example, to push your changes to the remote repository, you can use gpc:

```bash
gpc
```

This will run the git push origin HEAD command.

To see a list of all the shortcuts available, you can use the ogit command:

```bash
ogit
```

This will show a list of all the shortcuts defined in the git-shortcuts.sh file.

## License

Git Shortcuts is released under the **MIT License**. Feel free to use it, modify it, and share it with others.
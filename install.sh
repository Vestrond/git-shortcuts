#!/bin/bash
path=$(pwd)
cd || exit

if ! [ -f ~/.git-shortcuts-rc ]; then
    ln -s "$path/git-shortcuts.sh" .git-shortcuts-rc
else
    echo "Link to git-shortcuts.sh exists."
fi

if ! [ -f ~/.git-shortcuts-functions-rc ]; then
    ln -s "$path/git-shortcuts-functions.sh" .git-shortcuts-functions-rc
else
    echo "Link to git-shortcuts-functions.sh exists."
fi

gsr_line="source ~/.git-shortcuts-rc"
gsfr_line="source ~/.git-shortcuts-functions-rc"

if [ -f ~/.zshrc ]; then
    if grep -q "$gsr_line" ~/.zshrc && grep -q "$gsfr_line" ~/.zshrc; then
        echo ".zshrc is up-to-date."
    else
        echo "Add shortcuts to the .zshrc? (Y/n)"
        read answer

        if [[ "$answer" == "Y" || "$answer" == "y" ]]; then
          if ! grep -q "$gsr_line" ~/.zshrc; then
                echo "$gsr_line" >> ~/.zshrc
            fi
            if ! grep -q "$gsfr_line" ~/.zshrc; then
                echo "$gsfr_line" >> ~/.zshrc
            fi

            echo ".zshrc has been updated."
        else
          echo ".zshrc wouldn't be updated."
        fi
    fi
fi


if [ -f ~/.bashrc ]; then
    if grep -q "$gsr_line" ~/.bashrc && grep -q "$gsfr_line" ~/.bashrc; then
        echo ".bashrc is up-to-date."
    else
        echo "Add shortcuts to the .bashrc? (Y/n)"
        read answer

        if [[ "$answer" == "Y" || "$answer" == "y" ]]; then
            if ! grep -q "$gsr_line" ~/.bashrc; then
                echo "$gsr_line" >> ~/.bashrc
            fi
            if ! grep -q "$gsfr_line" ~/.bashrc; then
                echo "$gsfr_line" >> ~/.bashrc
            fi

            echo ".bashrc has been updated."
        else
          echo ".bashrc wouldn't be updated."
        fi
    fi
fi
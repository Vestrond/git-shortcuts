* Rename. Example: `gname "Vestrond <ocsho@yandex.ru>"`
* Add __gn__ for several commits. Example: `gn 2` or `gn 3`
* Add __gn__ for particular one old commit. Example: `gn -1` or `gn -5`

# Complex functions

# Get Jira Task (from branch)
function gjt() {
    branchName=$(gcrb)
    jiraTaskName=$(echo "$branchName" | grep -oE '[A-Z]+-[0-9]+')
    echo "$jiraTaskName"
}

# Git Commit Fix
function gfix() {
    commitText="fix: $1 [$(gjt)]"
    gm "$commitText"
}

# Git Commit Feature
function gfeat() {
    commitText="feat: $1 [$(gjt)]"
    gm "$commitText"
}

# Debug your commit line
function gfixc() {
    commitText="fix: $1 [$(gjt)]"
    echo "$commitText"
}

function gfeatc() {
    commitText="feat: $1 [$(gjt)]"
    echo "$commitText"
}